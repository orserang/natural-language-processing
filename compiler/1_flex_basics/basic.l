/*** 
 Purpose: Read word by word input to determine whether the
 text should be considered a float, integer, or var_name (string).
 Also, keeps track of the number of words and lines in the file.


 Definition section
 The C code in this section is ran before the yylex() loop
 is called. Thus, headers/files can be included, definitions
 can be made, and variables can be declared.
***/
%{
#include <iostream>

#define YY_DECL extern "C" int yylex()

int lineNum = 0;
int wordsNum = 0;

%}

/***
 Rules section
 Each line in this section is the regex followed by 
 the code executed in brackets when matched.
***/
%%

[ \t] 				   ; // ignore whitespace
\n    				   { ++lineNum; std::cout << std::endl; } // increment line num
[0-9]+\.[0-9]+  	   { std::cout << "Found a float: " << yytext << std::endl; ++wordsNum; }
[0-9]+          	   { std::cout << "Found an integer: " << yytext << std::endl; ++wordsNum; }
[a-zA-Z][a-zA-Z0-9]*   { std::cout << "Found a string: " << yytext << std::endl; ++wordsNum; }

%%

int main(int argc, char**argv) {

	// Calls the appropriate code from the above section
	// depending on the input received for each match in order.
	yylex();

	// Include the last line in the count if the file isn't empty
	if (wordsNum > 0)
		++lineNum;

	// Return the final results
	std::cout << std::endl << "Number of words: " << wordsNum << std::endl;
	std::cout << std::endl << "Number of lines: " << lineNum << std::endl;
}
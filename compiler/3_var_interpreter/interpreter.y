/***
 Purpose: Assign expression values to variables.
 Expressions can either be addition, subtraction,
 multiplication, or division between integers or
 previously assigned variables. The expressions
 should be in a hierarchy that interprets the 
 order of operations correctly. Be able to return
 the stored value of an assigned variable by calling
 the name as a single line command.
**/

%{
#include <iostream>
#include <map>
#include <cstring>

extern "C" int yylex();
extern "C" int yyparse();

void yyerror(const char *s);

// Helper function to compare const char * for map iterator
class StrCompare {
public:
  bool operator ()(const char*a, const char*b) const {
	return strcmp(a,b) < 0;
  }
};

std::map<char*, int, StrCompare> var_to_int;

%}

/*** union of all possible data return types from grammar ***/
%union {
	int iVal;
	char* sVal;
}

/*** Define token identifiers for flex regex matches ***/
%token NUM_I
%token NUM_F
%token VARNAME
%token ADD SUB MULT DIV EQUALS
%token LPAREN RPAREN
%token WS
%token EOL

/*** Define return type for grammar rules ***/
%type<iVal> atomicExpr
%type<iVal> assign
%type<iVal> expr
%type<iVal> addSubExpr
%type<iVal> multDivExpr

%type<iVal> NUM_I
%type<sVal> VARNAME

%%

start: expr EOL { 
				 printf("%i\n\n", $1); 
			 } start
	 | /* NULL */
	 ;

expr: addSubExpr
	| assign

addSubExpr: addSubExpr ADD multDivExpr {
			$$ = $1 + $3;
		  }
		  | addSubExpr SUB multDivExpr {
		  	$$ = $1 - $3;
		  }
		  | multDivExpr

multDivExpr: multDivExpr MULT atomicExpr {
				$$ = $1 * $3;
			}
			| multDivExpr DIV atomicExpr {
				if ($3 == 0) {
				  std::cerr << "ERROR: Cannot divide by 0!" << std::endl;
				  exit(1);
				}
				else {
			  	  $$ = $1 / $3;
				}
			}
			| atomicExpr

atomicExpr: VARNAME {
			  auto iter = var_to_int.find($1);

			  if (iter != var_to_int.end())
				$$ = iter->second; // variable is in var_to_int
			  else {
				std::cerr << "ERROR: variable " << $1 << " not found" << std::endl; 
				exit(1);
			  }
			}
			| NUM_I { 
					$$ = $1; 
				}
			| LPAREN expr RPAREN {
				$$ = $2;
			}

assign: VARNAME EQUALS expr {
			var_to_int[$1] = $3;
			$$ = var_to_int[$1];
		}

%%

int main(int argc, char **argv) {
	yyparse();
}

/* Display error messages */
void yyerror(const char *s) {
	printf("ERROR: %s\n", s);
}

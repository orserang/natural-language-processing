from random_word import RandomWords
from pronouncing import rhymes
from build_rhyme_vectors import *
import random


class rhyme_builder:
    def __init__(self):
        self.rhyme_list = []
        self.rhyme_sets = {}
        self.no_rhyme_list = []
        self.no_rhyme_sets = {}
        self.ran_word = RandomWords()
        with open("data/rhymes.txt", "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.split(",")
                if word1 not in self.rhyme_sets:
                    self.rhyme_sets[word1] = set()
                if word2 not in self.rhyme_sets:
                    self.rhyme_sets[word2] = set()
                self.rhyme_sets[word1].add(word2.lower())
                self.rhyme_sets[word2].add(word1.lower())

        with open("data/Non-rhymes.txt", "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.split(",")
                if word1 not in self.no_rhyme_sets:
                    self.no_rhyme_sets[word1] = set()
                if word2 not in self.no_rhyme_sets:
                    self.no_rhyme_sets[word2] = set()
                self.no_rhyme_sets[word1].add(word2.lower())
                self.no_rhyme_sets[word2].add(word1.lower())

    def add_rhymes(self, n):
        counter = 0
        does_rhyme = []

        while counter < n:
            new_word = self.get_valid_rand_word()
            print(new_word)

            new_rhymes = rhymes(new_word)  # if new_word has no rhymes try again
            if len(new_rhymes) == 0:
                continue
            print(new_word, new_rhymes)
            for i in range(min(5, len(new_rhymes))):
                new_rhyme_word_clean = new_rhymes[i].replace("'", "").replace("-", "")
                if new_rhymes[i].lower() != new_word:

                    if new_word not in self.rhyme_sets:
                        self.rhyme_sets[new_word] = set()
                    if new_rhyme_word_clean not in self.rhyme_sets:
                        self.rhyme_sets[new_rhyme_word_clean] = set()
                    if (
                        new_word in self.rhyme_sets[new_rhyme_word_clean]
                        or new_rhyme_word_clean in self.rhyme_sets[new_word]
                    ):
                        continue
                    self.rhyme_sets[new_word].add(new_rhyme_word_clean.lower())
                    self.rhyme_sets[new_rhyme_word_clean].add(new_word.lower())

                    does_rhyme.append(f"{new_word}, {new_rhyme_word_clean}")

                counter += 1

        with open("data/ai_rhymes.txt", "a") as f:
            for rhyme_pair in does_rhyme:
                f.write(rhyme_pair + "\n")

    def get_valid_rand_word(self):
        while True:
            new_word = self.ran_word.get_random_word()
            if new_word is None:
                continue
            if "-" in new_word or " " in new_word or "'" in new_word or "æ" in new_word:
                continue
            break
        return new_word.lower()

    def add_non_rhymes(self, n):
        counter = 0
        doesnt_rhyme = []
        set_words_added = set(self.rhyme_sets.keys())
        pairs_of_words = list(itertools.combinations(set_words_added, 2))
        random.shuffle(pairs_of_words)
        for new_word_a, new_word_b in pairs_of_words:

            new_word_a = new_word_a.strip()
            new_word_b = new_word_b.strip()
            if counter >= n:
                break
            if new_word_a == "" or new_word_b == "":
                continue
            if (
                new_word_a in self.rhyme_sets
                and new_word_b in self.rhyme_sets[new_word_a]
            ):
                continue

            print(new_word_a, new_word_b)
            doesnt_rhyme.append(f"{new_word_a}, {new_word_b}")
            counter += 1
        counter = 0
        while counter < n:
            new_word_a = self.get_valid_rand_word()
            new_word_b = self.get_valid_rand_word()

            if (
                new_word_a in self.rhyme_sets
                and new_word_b in self.rhyme_sets[new_word_a]
            ):
                continue

            print(new_word_a, new_word_b)
            doesnt_rhyme.append(f"{new_word_a}, {new_word_b}")
            counter += 1

        with open("data/ai_non_rhymes.txt", "a") as f:
            for rhyme_pair in doesnt_rhyme:
                f.write(rhyme_pair + "\n")


app = rhyme_builder()
app.add_rhymes(4000)
print("Rhyme Done")
app.add_non_rhymes(2000)

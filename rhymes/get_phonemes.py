from nltk.corpus import cmudict
import sys
import numpy as np
import edit_distance
import re
import itertools
import iteration_utilities

# edit distance to know rhymes
# edit 2 break rhymes in to sylibles
# 3 edit distance via phones

# todo: 50 rhyme pairs and 50 that do not, Check to find rhyme theshold
# todo: turn control set in to data set (word lengths,wordeditdistance, phoneme_edit_distance ave, wordt phone_edit, .. exc normalized versions )
# todo: scipy lib, support vector machine,
# todo: build matrix with matching colors of rhymes


class get_phonemes:
    def __init__(self):
        self.cmu = cmudict.dict()

    def get_most_similar_word(self, lower_word):
        """
        takes in any word if word is in
        :param word:
        :return: phone os similar word, similar word
        """
        # latex hypenation rules
        if lower_word in self.cmu:
            return lower_word

        minimum_change_value = len(lower_word)
        minimum_change_word = []
        for cmu_word in self.cmu:
            nl = edit_distance.edit_similarity(lower_word, cmu_word)

            if nl < minimum_change_value:
                minimum_change_value = nl
                minimum_change_word = [cmu_word]
            elif nl == minimum_change_value:
                minimum_change_word.append(cmu_word)

        return minimum_change_word

    def get_phonemes_from_list(self, list_of_word):
        phones = [self.cmu[i] for i in list_of_word]
        list_phones = []
        for word in list(itertools.product(*phones)):
            list_phones.append("".join(["".join(i) for i in word]))
        for i in range(len(list_phones)):
            list_phones[i] = "".join(i for i in list_phones[i] if not i.isdigit())
        return list_phones

    def phonemes_similarity(self, a, b):
        """
        :param a: phone of word a
        :param b: phone of word b
        :return: ratios of phones in a to b
        """

        counter = 0
        for a_phone in set(a):
            if a_phone in b:
                counter += 1
        return counter / len(b)

    # def get_edit_similarity_of_array_of_phone_lists(self, a, b):
    #     """
    #     :param a: (list) phonemes for word a
    #     :param b: (list) phonemes for word a
    #     :return: (float) 1 minus the edit distance divided by the sum of the lengths of a and b
    #     """
    #     return 1 - edit_distance.edit_similarity(tuple(a), tuple(b)) / (len(a) + len(b))

    def word_to_sortest_possible_phoneme_variations(self, word):
        list_of_of_all_subwords = self.r_word_to_all_possible_phoneme_variations(word)

        flattened_list_of_subwords = []
        for subword in list_of_of_all_subwords:
            flat_subword = list(iteration_utilities.deepflatten(subword, types=list))
            flattened_list_of_subwords.append(flat_subword)

        sorted_by_length = sorted(flattened_list_of_subwords, key=len)
        mean_len = np.mean([len(i) for i in sorted_by_length])

        filtered_by_shortest_length = [
            i for i in sorted_by_length if len(i) == len(sorted_by_length[0])
        ]
        return filtered_by_shortest_length

    def r_word_to_all_possible_phoneme_variations(self, word):
        """
        :param word: (str) word to be broken in to to sub words
        :param partial_word_threshold: (int) minimum length of the sub words
        :return: (list) list of sub words that are within the cmu dict
        """
        list_of_sub_strings = []
        if len(word) == 1:
            return word.lower()

        for i in range(1, len(word) + 1):
            prefix = word[:i]
            suffix = word[i:]
            if prefix in self.cmu:
                suffix_phone = self.r_word_to_all_possible_phoneme_variations(suffix)
                for sp_list in suffix_phone:
                    temp = [prefix]
                    temp.append(sp_list)
                    list_of_sub_strings.append(temp)
                if len(prefix) == len(word):
                    list_of_sub_strings.append([prefix])
        return list_of_sub_strings


def main(sargs):
    app = get_phonemes()

    if len(sargs) == 2:
        word1 = sargs[0]
        word2 = sargs[1]
        a = app.word_to_sortest_possible_phoneme_variations(word1)
        b = app.word_to_sortest_possible_phoneme_variations(word2)
        for i in a:

            print(word1, i)
        for i in b:
            print(word1, i)
    else:
        print("useage get_phonemes.py <word1> <word2>  <threshold (optional)>")


if __name__ == "__main__":
    main(sys.argv[1:])

from hyphenate import hyphenate_word
import pronouncing
import cmudict
import Phyme
import nltk


def SplitWordsByHyphens(word):
    return hyphenate_word(word)


def SplitWordsInToPhones(word):
    return pronouncing.phones_for_word(word)[0].split(" ")


if __name__ == "__main__":
    RB = Phyme.Phyme()
    a = "bow"

    nl = nltk.edit_distance(a, "killing")
    print(nl)
    b = "bowling"
    # print(RB.get_family_rhymes(a))
    app = Rhymes()
    result = app.does_word_rhyme_with_other_word(a, b, end_rhyme=False)
    print(result)

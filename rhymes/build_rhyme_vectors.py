from get_phonemes import *
import numpy as np
from operator import itemgetter
import sys, os
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
import nltk
import pickle
import networkx


#### Write up a summery of the project next semester  1.5 pages?
#       Demo output of cool stuff happening Graph
#    livesvm - weight missclassification
#   Beta = .5
#   alpha = .0385
#   DUE DATE EMAIL IN PAPER
# /

class rhyme_vector:
    def __init__(self, state="load"):
        self.rhyme_list = []
        self.no_rhyme_list = []
        self.app = get_phonemes()

        if state == "load":
            self.load_data()
        else:
            self.save_data()

    def load_data(self):
        if os.path.isfile("save.p"):
            loaded_data = pickle.load(open("save.p", "rb"))
            self.x = loaded_data["x"]
            self.y = loaded_data["y"]
            print("data Loaded")
        else:
            self.save_data()

    def save_data(self):
        with open("data/rhymes.txt", "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.lower().split(",")
                self.rhyme_list.append(tuple((word1, word2.strip())))

        with open("data/Non-rhymes.txt", "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.lower().split(",")
                self.no_rhyme_list.append(tuple((word1, word2.strip())))

        try:
            with open("data/ai_rhymes.txt", "r") as f:
                for line in f:
                    if line == "":
                        continue
                    word1, word2 = line.lower().split(",")
                    self.rhyme_list.append(tuple((word1, word2.strip())))

            with open("data/ai_non_rhymes.txt", "r") as f:
                for line in f:
                    if line == "":
                        continue
                    word1, word2 = line.lower().split(",")
                    self.no_rhyme_list.append(tuple((word1, word2.strip())))
        except:
            pass
        self.build_vectors()
        save_data = {"x": self.x, "y": self.y}
        pickle_out = open("save.p", "wb")
        pickle.dump(save_data, pickle_out)
        pickle_out.close()

        print("data saved")

    def does_a_rhyme_with_b(self, a, b):
        return self.model.predict(
            [list(self.get_rhyme_vector(a, b)) + list(self.get_rhyme_vector(b, a))]
        )

    def get_sub_word_similarity_distance(self, a_list, b_list):

        values = []
        for phone_a in a_list:
            for phone_b in b_list:
                values.append(
                    edit_distance.edit_similarity(tuple(phone_a), tuple(phone_b))
                )
        values = np.array(values)
        return (
            max(values, key=itemgetter(0)),
            min(values, key=itemgetter(0)),
            max(values, key=itemgetter(1)),
            min(values, key=itemgetter(1)),
            np.mean(values[:, 0]),
            np.mean(values[:, 1]),
        )

    def get_phone_edit_distance_list(self, a_list, b_list):

        values = []
        for phone_a in a_list:
            for phone_b in b_list:
                values.append(
                    nltk.edit_distance(
                        tuple(phone_a), tuple(phone_b), substitution_cost=2
                    )
                )
        return np.mean(values), np.max(values), np.min(values)

    def build_vectors(self):
        self.rhyming_vectors = []
        self.non_rhyming_vectors = []
        for a, b in self.rhyme_list:

            self.rhyming_vectors.append(
                list(self.get_rhyme_vector(a, b)) + list(self.get_rhyme_vector(b, a))
            )
        for a, b in self.no_rhyme_list:
            self.non_rhyming_vectors.append(
                list(self.get_rhyme_vector(a, b)) + list(self.get_rhyme_vector(b, a))
            )
        self.x = self.rhyming_vectors + self.non_rhyming_vectors
        self.y = [0] * len(self.rhyming_vectors) + [1] * len(self.non_rhyming_vectors)

    def count_syllables(self, word):
        return len(
            re.findall("(?!e$)[aeiouy]+", word, re.I)
            + re.findall("^[^aeiouy]*e$", word, re.I)
        )

    def get_rhyme_vector(self, a, b):

        partial_word_list_for_a = self.app.word_to_sortest_possible_phoneme_variations(
            a
        )
        partial_word_list_for_b = self.app.word_to_sortest_possible_phoneme_variations(
            b
        )
        # print(a,b)
        a_syllables_count = self.count_syllables(a)
        b_syllables_count = self.count_syllables(b)
        syllable_count_ave = (a_syllables_count + b_syllables_count) / 2.0

        a_len = len(a)
        b_len = len(b)
        a_b_avg_len = (a_len + b_len) / 2

        partial_phonemes_list_for_a = []
        partial_phonemes_list_for_b = []

        (
            a_b_sim_distance_max,
            a_b_sim_distance_min,
            a_b_sim_distance_length_max,
            a_b_sim_distance_length_min,
            a_b_sim_distance_avg,
            a_b_sim_distance_length_avg,
        ) = self.get_sub_word_similarity_distance(
            partial_word_list_for_a, partial_word_list_for_b
        )

        for phoneme_a in partial_word_list_for_a:
            temp = self.app.get_phonemes_from_list(phoneme_a)
            for i in temp:
                partial_phonemes_list_for_a.append(i)

        for phoneme_b in partial_word_list_for_b:
            temp = self.app.get_phonemes_from_list(phoneme_b)
            for i in temp:
                partial_phonemes_list_for_b.append(i)

        (
            a_b_phoneme_avg,
            a_b_phoneme_max,
            a_b_phoneme_min,
        ) = self.get_phone_edit_distance_list(
            partial_phonemes_list_for_a, partial_phonemes_list_for_b
        )
        (
            a_b_sim_phones_distance_max,
            a_b_sim_phones_distance_min,
            a_b_sim_phones_distance_length_max,
            a_b_sim_phones_distance_length_min,
            a_b_sim_phones_distance_avg,
            a_b_sim_phones_distance_length_avg,
        ) = self.get_sub_word_similarity_distance(
            partial_phonemes_list_for_a, partial_phonemes_list_for_b
        )

        # print(a,b)
        length_of_sim_suffix = edit_distance.length_of_similar_suffix(a, b)

        normalised_length_of_sim_suffix = length_of_sim_suffix / min([a_len, b_len])

        return (
            a_syllables_count,
            b_syllables_count,
            syllable_count_ave,
            length_of_sim_suffix,
            normalised_length_of_sim_suffix,
            # a_len,
            # b_len,
            # a_b_avg_len,
            a_b_sim_distance_max[0],  # value
            a_b_sim_distance_max[1],  # distance
            a_b_sim_distance_min[0],  # value
            a_b_sim_distance_min[1],  # distance
            a_b_sim_distance_length_max[0],  # value
            a_b_sim_distance_length_max[1],  # distance
            a_b_sim_distance_length_min[0],  # value
            a_b_sim_distance_length_min[1],  # distance
            a_b_sim_phones_distance_max[0],  # value
            a_b_sim_phones_distance_max[1],  # distance
            a_b_sim_phones_distance_min[0],  # value
            a_b_sim_phones_distance_min[1],  # distance
            a_b_sim_phones_distance_length_max[0],  # value
            a_b_sim_phones_distance_length_max[1],  # distance
            a_b_sim_phones_distance_length_min[0],  # value
            a_b_sim_phones_distance_length_min[1],  # distance
            a_b_sim_distance_length_avg,
            a_b_sim_distance_avg,
            a_b_phoneme_avg,
            a_b_phoneme_max,
            a_b_phoneme_min,
        )

    def read_in_training(self, filename):
        x_test_words = []
        y_test = []
        with open(filename[0], "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.split(",")
                x_test_words.append(tuple((word1.lower(), word2.strip().lower())))
                y_test.append(0)
        with open(filename[1], "r") as f:
            for line in f:
                if line == "":
                    continue
                word1, word2 = line.split(",")
                x_test_words.append(tuple((word1.lower(), word2.strip().lower())))
                y_test.append(1)
        x_test_vectors = []
        for word_a, word_b in x_test_words:
            x_test_vectors.append(
                list(self.get_rhyme_vector(word_a, word_b))
                + list(self.get_rhyme_vector(word_b, word_a))
            )
        return x_test_vectors, y_test

    def train_ml(self, c, training=None):
        if training is None:
            self.model = SVC(C=c,kernel='rbf',gamma='auto',degree=9,class_weight={0:.009, 1:.991})
            self.model.fit(self.x, self.y)
            return

        if training == "split":
            x_train, x_test, y_train, y_test = train_test_split(
                self.x, self.y, test_size=0.10
            )
        else:
            x_train = self.x
            y_train = self.y
            x_test, y_test = self.read_in_training(training)
        self.model = SVC(C=c,kernel='rbf',gamma='auto',degree=9,class_weight={0:.009, 1:.991})

        self.model.fit(x_train, y_train)

        y_pre = self.model.predict(x_test)
        return confusion_matrix(y_pre, y_test, normalize=None)

    def confusion_matrix_to_score(self, c):
        # using this scoring system previous score before last class was  0.7534784921215425

        TP = c[0, 0]  # True Positive
        FP = c[0, 1]  # False Positive Type 1
        FN = c[1, 0]  # False Negative
        TN = c[1, 1]  # True Negative type 2
        TPR = (1- TP / (TP + FN) ) # True Positive Rate
        TNR = (1- TN / (FP + TN))  # True Negative Rate

        # 40000 * alpha + 81 * beta
        alpha = FP/(FP+TN)
        beta = FN/(TP+FN)
        print("alpha", alpha)
        print("beta",beta)

        return (alpha * (FP + TN) + beta * (TP + FN))


        ## Other Scoring systems Tried

        # return (TPR+TNR)/2  # Balanced Accuracy Rate

        # denom = np.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN))
        # return ((TP*TN)-(FP*FN))/ denom# The Matthews correlation coefficient (MCC) or phi coefficient

        # return (TP+TN)/(TP+TN+FP+FN) # accuracy

        # return (2 * TP) / (2 * TP + FP + FN)# F1 is the harmonic mean of precision and sensitivity:


def main(argv):

    if len(argv) == 3:
        state = argv[0]
        training = argv[1:]
    elif len(argv) == 1:
        state = argv[0]
        training = "split"
    else:
        print(
            "usage build_rhyme_vector.py <load or save>  optional ( <rhyme_test_file> <no_rhyme_test_file> )"
        )
        return
    best_score = np.inf
    best_c = np.inf
    c_values = [.074]

    # c_values = [0.05, 0.1, 0.5, 1, 5, 10, 50, 100, 500, 1000]

    app = rhyme_vector(state)
    data = {}
    print("training started")
    for c in c_values:

        c_matrix = app.train_ml(c, training)
        c = str(c)
        data[c] = c_matrix
        print(c,'\n',c_matrix)
        accuracy = app.confusion_matrix_to_score(data[c])
        print("score",accuracy)
        if accuracy < best_score:
            best_score = accuracy
            best_c = c
    print(f"Best C value is {best_c} with accuracy of  {data[best_c]}  {best_score}")


if __name__ == "__main__":
    main(sys.argv[1:])

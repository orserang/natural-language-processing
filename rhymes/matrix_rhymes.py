import get_rhymes
import re
from graph import *
from build_rhyme_vectors import *
import matplotlib.pyplot as plt

import networkx as nx

import num2word


class BuildRhymingMatrix:
    def __init__(self, fname,c):
        read_in_lines = open(fname).readlines()
        self.array_of_lines_song = []
        self.rhyme_vector = rhyme_vector()
        self.rhyme_vector.train_ml(c=c)
        self.rhymer = get_rhymes.Rhyming()
        for i in range(len(read_in_lines)):
            if read_in_lines[i].strip() != "\n":
                words_in_line = [
                    "".join(
                        num2word.word(e) if e.isdigit() else e
                        for e in word
                        if e.isalnum()
                    )
                    for word in read_in_lines[i].split(" ")
                ]
                self.array_of_lines_song.append(" ".join(words_in_line))
        self.array_of_lines_song = np.array(self.array_of_lines_song)

    def count_syllables(self, word):
        return len(
            re.findall("(?!e$)[aeiouy]+", word, re.I)
            + re.findall("^[^aeiouy]*e$", word, re.I)
        )

    def initialize_matrix(self):
        print(self.array_of_lines_song.shape)
        number_of_lines = self.array_of_lines_song.shape[0]
        self.max_number_of_syllable = max(
            [self.count_syllables(line) for line in self.array_of_lines_song]
        )
        self.max_number_of_words = max(
            [len(line.split()) for line in self.array_of_lines_song]
        )
        print(number_of_lines, self.max_number_of_syllable, self.max_number_of_words)
        self.image_matrix = np.ones(
            (number_of_lines, self.max_number_of_syllable), dtype=np.int
        )*-2

    def link_rhyming_words_with_connected_comp(self, threshold=0.98):

        all_word_set = set()
        words_in_song_and_rhyme_value = {}
        for line_number in range(self.array_of_lines_song.shape[0]):
            line = self.array_of_lines_song[line_number].split(" ")
            # print(line)

            for word_number in range(len(line)):
                word = line[word_number].lower()

                if word == "" or word in all_word_set:
                    continue  # if line is blank skip or of word is already in set

                for key in all_word_set:
                    rhyme_value = self.rhyme_vector.does_a_rhyme_with_b(word, key)
                    # print(key, word, rhyme_value)
                    if rhyme_value[0] == 0:
                        if key not in words_in_song_and_rhyme_value:
                            words_in_song_and_rhyme_value[key] = {}
                        words_in_song_and_rhyme_value[key][word] = rhyme_value[0]
                        if word not in words_in_song_and_rhyme_value:
                            words_in_song_and_rhyme_value[word] = {}
                        words_in_song_and_rhyme_value[word][key] = rhyme_value[0]
                all_word_set.add(word)

        self.list_of_all_rhyming_words = list(words_in_song_and_rhyme_value.keys())

        G = nx.Graph()
        # G.add_nodes_from(range(len(words_in_song_and_rhyme_value)))

        self.my_graph = Graph(len(words_in_song_and_rhyme_value))
        print("\nWord number, Words, Words that rhyme and values")
        for key in words_in_song_and_rhyme_value.keys():
            key_index = self.list_of_all_rhyming_words.index(key)
            for rhymes_with_key in words_in_song_and_rhyme_value[key].keys():
                rhymes_with_key_index = self.list_of_all_rhyming_words.index(
                    rhymes_with_key
                )
                G.add_edge(
                    key,
                    rhymes_with_key,
                    weight=words_in_song_and_rhyme_value[key][rhymes_with_key],
                )
                self.my_graph.addEdge(key_index, rhymes_with_key_index)
            print(key_index, key, words_in_song_and_rhyme_value[key])
        self.list_of_cc = self.my_graph.connectedComponents()
        nx.draw_networkx(G, with_labels=self.list_of_all_rhyming_words)
        plt.axis("off")

        plt.show()
        G = nx.enumerate_all_cliques(G)
        # print("All cliques")
        # for g in G:
        #     if len(g) > 1:
        #         print(g)

        plt.show()

    def print_cc_groups(self):
        print("\n")
        for connected_component in self.list_of_cc:
            print(connected_component, end="\t")
            for node in connected_component:
                print(self.list_of_all_rhyming_words[node], end=" ")
            print()

    def get_cc_group_number(self, word):
        """

        :param word: single word
        :return:    returns -1 if word not part of the connected component
                    returns connected component index starting at 1
        """
        if word not in self.list_of_all_rhyming_words:
            return -1
        word_index_value = self.list_of_all_rhyming_words.index(word)
        for i in range(len(self.list_of_cc)):
            if word_index_value in self.list_of_cc[i]:
                return i + 3

        raise Exception(
            "If you're seeing this, the code is in what I thought was an unreachable state. "
            "I could give you advice for what to do. "
            "But honestly, why should you trust me? "
            "I clearly screwed this up. "
            "I'm writing a message that should never appear, yet I know it will probably appear someday."
            "On a deep level, I know I'm not up to this task. I'm so sorry."
        )

    def build_matrix_with_rhymes_based_on_syl_count(self):

        for line_number in range(self.array_of_lines_song.shape[0]):
            line = self.array_of_lines_song[line_number].split(" ")
            line_syl_count = 0
            for word_number in range(len(line)):
                word = line[word_number].lower()
                word_syl_count = self.count_syllables(word)
                for i in range(line_syl_count, line_syl_count + word_syl_count+1):
                    self.image_matrix[line_number, i] = self.get_cc_group_number(word)
                line_syl_count += word_syl_count
        print("\nRhyming matrix")
        print(self.image_matrix)

        cmaps = ['nipy_spectral']
        for map in cmaps:
            print(map)
            plt.matshow(self.image_matrix,cmap=map)
            plt.show()


def main(args):
    if len(args) == 2:
        app = BuildRhymingMatrix(args[0],float(args[1]))
        app.initialize_matrix()
        app.link_rhyming_words_with_connected_comp()
        app.print_cc_groups()
        app.build_matrix_with_rhymes_based_on_syl_count()
    else:
        print("useage: <corpus> <c> ")


if __name__ == "__main__":
    main(sys.argv[1:])

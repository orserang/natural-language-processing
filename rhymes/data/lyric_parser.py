import num2word
import itertools
temp = """'Cause sometimes you just feel tired, you feel weak
And when you feel weak you feel like you want to just give up

But you gotta search within you, you gotta find that inner strength
And just pull that shit out of you and get that motivation to not give up
And not be a quitter, no matter how bad you want to just fall flat on your face and collapse

'Til I collapse I'm spilling these raps long as you feel 'em
'Til the day that I drop you'll never say that I'm not killing 'em
'Cause when I am not then I'm a stop pinning them
And I am not hip-hop and I'm just not Eminem
Subliminal thoughts when I'm stop sending them
Women are caught in webs spin and hock venom
Adrenaline shots of penicillin could not get the illin' to stop
Amoxicillin is just not real enough
The criminal cop killing hip-hop filling a
Minimal swap to cop millions of Pac listeners
You're coming with me, feel it or not
You're gonna fear it like I showed you the spirit of god lives in us
You hear it a lot, lyrics that shock, is it a miracle
Or am I just a product of pop fizzing up
For shizzle my whizzle this is the plot listen up
You Bizzles forgot Slizzle does not give a fuck

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

Music is like magic there's a certain feeling you get
When you're real and you spit and people are feeling your shit
This is your moment and every single minute you spittin'
Trying to hold onto it 'cause you may never get it again
So while you're in it try to get as much shit as you can
And when your run is over just admit when it's at its end
'Cause I'm at the end of my wits with half the shit that gets in
I got a list, here's the order of my list that it's in;
It goes, Reggie, Jay-Z, Tupac and Biggie
Andre from Outkast, Jada, Kurupt, Nas and then me
But in this industry I'm the cause of a lot of envy
So when I'm not put on this list the shit does not offend me
That's why you see me walk around like nothing's bothering me
Even though half you people got a fuckin' problem with me
You hate it but you know respect you've got to give me
The press's wet dream like Bobby and Whitney, Nate hit me

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

Soon as a verse starts I eat at an MC's heart
What is he thinking? Enough to not go against me, smart
And its absurd how people hang on every word
I'll probably never get the props I feel I ever deserve
But I'll never be served my spot is forever reserved
If I ever leave earth that would be the death of me first
'Cause in my heart of hearts I know nothing could ever be worse
That's why I'm clever when I put together every verse
My thoughts are sporadic, I act like I'm an addict
I rap like I'm addicted to smack like I'm Kim Mathers
But I don't want to go forth and back in constant battles
The fact is I would rather sit back and bomb some rappers'
So this is like a full blown attack I'm launching at 'em
The track is on some battling raps who want some static
'Cause I don't really think that the fact that I'm Slim matters
A plaque of platinum status is whack if I'm not the baddest

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

'Til the roof comes off, till the lights go out
'Til my legs give out, can't shut my mouth
'Til the smoke clears out and my high perhaps
I'm a rip this shit till my bone collapse

Until the roof (Until the roof)
The roof comes off (The roof comes off)
Until my legs (Until my legs)
Give out from underneath me (Underneath me, I)

I will not fall
I will stand tall drop
Feels like no one can beat me""".replace('\n', ' ').replace('(','').replace(')','').replace(',','').replace('-','').replace("'",'').replace(';','').replace('?','')

word_rhyme_sets = {}
word_set = set()

for word in temp.split(' '):
    word = num2word.word(word) if word.isnumeric() else word
    word_set.add(word.lower())
with open("rap_rhymes.txt", "r") as f:
    for line in f:
        word1, word2 = line.lower().split(',')
        if word1 not in word_rhyme_sets:
            word_rhyme_sets[word1] = set()
        if word2 not in word_rhyme_sets:
            word_rhyme_sets[word2] = set()
        word_rhyme_sets[word1].add(word2)
        word_rhyme_sets[word2].add(word1)


all_pairs = list(itertools.combinations(word_set, 2))
non_rhyme_pairs = []
with open("rap_non_rhymes.txt", "w") as f:
    for pair in all_pairs:
        if '' in pair:
            continue
        if pair[0] in word_rhyme_sets and  pair[1] in word_rhyme_sets[pair[0]]:
            continue
        f.write(
            f'{pair[0]}, {pair[1]}\n')


with open("rap_non_rhymes_no_rhyming_words.txt", "w") as f:
    for pair in all_pairs:
        if '' in pair:
            continue
        if pair[0] in word_rhyme_sets or  pair[1] in word_rhyme_sets:
            continue
        f.write(f'{pair[0]}, {pair[1]}\n')
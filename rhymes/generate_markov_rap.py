import collections
import get_syllable
import numpy as np
import nltk
import sys, os
from collections import Counter
import get_rhymes
import get_syllable


def load_sentences(fname):
    if os.path.isdir(fname):
        lines = []
        print(type(fname), fname)
        for file_name in os.listdir(fname):
            print(file_name)
            lines += open(fname + file_name).readlines()

    else:
        lines = open(fname).readlines()
    for i in range(len(lines)):
        lines[i] = " ".join(lines[i].split(" ")[::-1]).replace("\n", "") + "."

    temp = []
    for line in lines:
        if line != ".":
            temp.append(line)
    data = " ".join(temp)
    sentences = nltk.sent_tokenize(data)

    return list(sentences)


def estimate_prior(all_books_sentences, k):
    starting_words = []

    book_s = []
    for i in range(len(all_books_sentences)):
        book_s = book_s + [s for s in all_books_sentences[i]]
    padding = ["$PADDING$"] * k
    for s in book_s:
        temp = s.split(" ")

        temp = [i for i in temp if i]
        if len(temp) == 0:
            continue
        if len(temp) <= k:
            temp = temp + padding
        starting_words.append(" ".join(temp[: k + 1]))
    starting_s_dict = {}
    for words in starting_words:

        temp = tuple(words.split(" ")[:k])
        if temp[:k] in starting_s_dict:
            starting_s_dict[temp[:k]] = +1

        else:
            starting_s_dict[temp[:k]] = 1

        starting_s_dict = count_to_cumsum(
            collections.OrderedDict(sorted(starting_s_dict.items()))
        )
        # if len(starting_s_dict[s])>1:
    # print(starting_s_dict)
    return starting_s_dict


def estimate_transition_matrix(all_books_sentences, k):
    padding = ["$PADDING$"] * k
    starting_words = []

    book_s = []
    for i in range(len(all_books_sentences)):
        book_s = book_s + [
            s.replace("\n", "").replace("\t", "") for s in all_books_sentences[i]
        ]

    for s in book_s:
        temp = s.split(" ")

        temp = [i for i in temp if i]
        if len(temp) == 0:
            continue
        starting_words.append(" ".join(temp))
    word_counter = {}
    for i in range(0, len(starting_words)):
        temp = starting_words[i].split(" ")
        for j in range(len(temp)):
            if temp[j] == "":
                continue
            temp = temp + padding
            key = tuple(temp[j : j + k])
            if key not in word_counter:
                word_counter[key] = {}
            values = temp[j + k]

            if tuple(values) not in word_counter[key]:
                word_counter[key][values] = 1
            else:
                word_counter[key][values] = word_counter[key][values] + 1
    for word in word_counter:
        temp = collections.OrderedDict(sorted(word_counter[word].items()))

        word_counter[word] = count_to_cumsum(temp)
        # if len(word_counter[word]) >1:
        #   print(word,word_counter[word])
    return word_counter


def estimate_syllable_length_dist(all_sentences):
    s_lengths = []
    for i in range(len(all_sentences)):
        s_lengths = s_lengths + [
            get_syllable.get_syllable_count(s) for s in all_sentences[i]
        ]
    length_counter = dict(Counter(s_lengths))
    length_counter = collections.OrderedDict(sorted(length_counter.items()))
    length_counter = count_to_cumsum(length_counter)
    return length_counter


def estimate_sentence_length_dist(all_books_sentences):
    book_lengths = []
    for i in range(len(all_books_sentences)):
        book_lengths = book_lengths + [
            len(s.split(" ")) for s in all_books_sentences[i]
        ]
    length_counter = dict(Counter(book_lengths))
    length_counter = collections.OrderedDict(sorted(length_counter.items()))
    length_counter = count_to_cumsum(length_counter)
    return length_counter


def count_to_cumsum(ordered_dict):
    my_sum = sum(ordered_dict.values())
    temp = 0
    for key in ordered_dict:
        ordered_dict[key] = temp = temp + (ordered_dict[key] / my_sum)
    return ordered_dict


def generate_text(
    k_word_and_prior_prob_s,
    k_prev_words_to_next_word_and_prob,
    sentence_syllable_length_and_final_k_words_to_prob,
    num_sentences_to_generate,
    k,
):
    text = []
    app = get_rhymes.get_rhymes()
    padding = tuple(["$PADDING$"] * k)
    print("\n")
    counter = 0
    print(f"Generating {num_sentences_to_generate} sentences")
    while counter <= num_sentences_to_generate:
        for j in range(2):
            if j == 0:
                print()
                s_len = pick_random_dict_key(
                    sentence_syllable_length_and_final_k_words_to_prob
                )
                s_len = 12
                s = np.array(pick_random_dict_key(k_word_and_prior_prob_s))
                rhyming_word = s[0]
            else:
                rhyme_list = app.word_rhymes(rhyming_word)
                for i in range(1000):
                    s = np.array(pick_random_dict_key(k_word_and_prior_prob_s))
                    if s[0] in rhyme_list or s[0] == rhyming_word:
                        # print(f'~~~~Found Rhyming Word {s} with {rhyming_word}')
                        break
            # print(f"S Length {s_len}")
            while get_syllable.get_syllable_count(" ".join(s)) <= s_len:
                key = tuple(s[-k:])
                if key == padding:
                    s = s[:-k]
                    # print("Padding stop")
                    break
                s = np.append(
                    s,
                    (
                        np.array(
                            pick_random_dict_key(
                                k_prev_words_to_next_word_and_prob[key]
                            )
                        )
                    ),
                )
            # print(f"s {s} \n s length {get_syllable.get_syllable_count(' '.join(s[::-1]))}")
            print(
                " ".join(s[::-1]).replace("$PADDING$", " ").replace(".", ""),
                "\t",
                get_syllable.get_syllable_count(" ".join(s)),
            )
        counter += 1

        text = np.append(text, s)


def pick_random_dict_key(my_dict):
    rand_value = np.random.random()
    for word in my_dict:
        if rand_value < my_dict[word]:
            return word


def main(args):
    if len(args) >= 3:
        corpus_fnames = args[:-2]
        # order k markov model looks back at k previous words:
        k = int(args[-2])
        num_sentences_to_generate = int(args[-1])

        all_books_sentences = [load_sentences(f) for f in corpus_fnames]
        k_word_and_prior_prob_s = estimate_prior(all_books_sentences, k)

        sentence_syllable_length = estimate_syllable_length_dist(all_books_sentences)

        k_prev_words_to_next_word_and_prob = estimate_transition_matrix(
            all_books_sentences, k
        )

        sentence_length_and_final_k_words_to_prob = estimate_sentence_length_dist(
            all_books_sentences
        )

        generate_text(
            k_word_and_prior_prob_s,
            k_prev_words_to_next_word_and_prob,
            sentence_syllable_length,
            num_sentences_to_generate,
            k,
        )

    else:
        print(
            "usage: <corpus_1.txt> [corpus_2.txt...] <k_order_of_markov_model> <num_sentences_to_generate>"
        )


if __name__ == "__main__":
    main(sys.argv[1:])


# send fake jane austin

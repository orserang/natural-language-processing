# importing all necessary modules
from nltk.tokenize import sent_tokenize, word_tokenize
import gensim, os
from gensim.models import Word2Vec
import sys
import numpy as np


class WordVector:
    def __init__(self, corpus):
        flist = []
        for iterated_corpus in corpus:

            if os.path.isdir(iterated_corpus):
                for filename in os.listdir(iterated_corpus):
                    flist.append(iterated_corpus + filename)
            else:
                flist.append(iterated_corpus)
        lines = []
        for fname in flist:
            with open(fname, "r") as f:
                lines.append(f.read().replace("\n", " "))

        lines = " ".join(np.r_[lines])

        # iterate through each sentence in the file
        data = []
        for i in sent_tokenize(lines):
            temp = []
            # tokenize the sentence into words
            for j in word_tokenize(i):
                temp.append(j.lower())
            data.append(temp)

        data = np.array(data).flatten()
        self.model1 = gensim.models.Word2Vec(
            data, min_count=1, size=1000, window=5
        )  # uses CBOW
        self.model2 = gensim.models.Word2Vec(
            data, min_count=1, size=1000, window=5, sg=1
        )  # uses skip-graph

    def compair_words(self, word1, word2):
        if self.model1.wv.__contains__(word1) and self.model1.wv.__contains__(word2):
            print(
                "CBOW similarity between ",
                word1,
                " and ",
                word2,
                self.model1.wv.similarity(word1, word2),
            )
        else:
            print(f"{word1} or {word2} is not in the model")
            return None, None
        if self.model2.wv.__contains__(word1) and self.model2.wv.__contains__(word2):
            print(
                "Skip-graph similarity between",
                word1,
                " and ",
                word2,
                self.model2.wv.similarity(word1, word2),
            )
        else:
            print(f"{word1} or {word2} is not in the model")
            return None, None
        return self.model1.wv.similarity(word1, word2), self.model2.wv.similarity(
            word1, word2
        )


if __name__ == "__main__":
    if len(sys.argv) >= 4:
        temp = [*sys.argv[1:-2]]
        word1 = sys.argv[-2].lower()
        word2 = sys.argv[-1].lower()
        app = WordVector(temp)
        app.compair_words(word1, word2)
    else:
        print("Useage <corpus> ...  Word1 Word2")

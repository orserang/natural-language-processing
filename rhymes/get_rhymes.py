from split_words_hypens import *
from pronouncing import rhymes
import numpy as np
from nltk import edit_distance  #### to find close words
import get_syllable
import sys

# Todo:  Edit distance from NLTK.Editdistance
# Todo: remove empty lines
# def does_word_rhyme_with_other_word(a,b,percent_to_rhyme=.5):
#     a_phon = hyphenate_word(a)
#     b_phon = hyphenate_word(b)
#     attempts = matches = 0
#     for ax,bx in zip(a_phon[::-1],b_phon[::-1]):
#         attempts += 1
#         if bx in self.phoneme_to_rhymes(ax):
#             matches +=1
#     return matches/attempts >= percent_to_rhyme


class Rhyming:
    def __init__(self):
        pass

    def get_words_that_rhyme_with(self, word):
        return list(rhymes(word) + rhymes(SplitWordsByHyphens(word)[-1]))

    def get_word_that_rhymes_with_input_givin_sylible_count(self, word, count):
        return np.array(
            [word for word in get_syllable.get_syllable(self.word_rhymes(word), count)]
        )


# Unfinished class to take over role of Rhyming
class New_Rhym_Class:
    def __init__(self):
        pass

    def does_word_rhyme_with_other_word(self, a, b, threshold=0.2, end_rhyme=True):

        if end_rhyme:
            order = -1
        else:
            order = 1

        a_phone = pronouncing.phones_for_word(a)
        b_phone = pronouncing.phones_for_word(b)

        if len(a_phone) == 0 or len(b_phone) == 0:
            return
        a_phone_list = a_phone[0].split(" ")
        b_phone_list = b_phone[0].split(" ")
        print(b_phone_list, a_phone_list)
        phone_matches = 0
        # for a_x in a_phone_list:
        #     for b_x in b_phone_list:
        #         if a_x == b_x:
        #             phone_matches += 1

        for a_x, b_x in zip(a_phone_list[::order], b_phone_list[::order]):
            if a_x == b_x:
                phone_matches += 1
        print(phone_matches)
        return phone_matches / min(len(a_phone_list), len(b_phone_list))


if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "load":
        app = Rhyming()
        while True:
            word = input("Enter a word: ")
            count = int(input("Enter number of syllables (0 to exit): "))
            if count < 1:
                break
            print(
                "These words Rhyme with {} and have {} syllable(s)\n{}".format(
                    word,
                    count,
                    app.get_word_that_rhymes_with_input_givin_sylible_count(
                        word, count
                    ),
                )
            )
    elif len(sys.argv) > 1:
        app = Rhyming()
        for i in range(1, len(sys.argv), 2):
            word = sys.argv[i]
            count = int(sys.argv[i + 1])
            print(
                "These words Rhyme with {} and have {} syllable(s)\n{}".format(
                    word,
                    count,
                    app.get_word_that_rhymes_with_input_givin_sylible_count(
                        word, count
                    ),
                )
            )
    else:
        print("Useage  <word> <number of syllables>")

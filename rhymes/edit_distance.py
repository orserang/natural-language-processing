import nltk, sys
import numpy as np
from operator import itemgetter


class Memoized:
    def __init__(self, function):
        self._function = function
        self._cache = {}

    def __call__(self, *args):
        if args not in self._cache:
            self._cache[args] = self._function(*args)
        # the result must now be in the cache:
        return self._cache[args]


def edit_similarity(word1, word2):
    value = r_edit_similarity_path_length(word1, word2)
    # print(value)

    return value


@Memoized
def r_edit_similarity_path_length(word1, word2, i=None, j=None, value=None):

    """
    Calculate the minimum Levenshtein like edit-distance no penalty for adding words to the start or end  of word 1
    :param word1: user inputted word
    :param word2: cmu word
    :param i: index value word1
    :param j: index value of word2
    :return: float minimum edit distance
    """
    if i is None:
        i = len(word1) - 1
    if j is None:
        j = len(word2) - 1
    if value is None:
        value = 0

    if i < 0 or j < 0:
        return 0, 0

    c1 = word1[i]
    c2 = word2[j]

    word1_skip_value = -1
    word2_skip_value = -1
    substitution_value = -1* np.sqrt(2)
    no_substitution_value = 1
    word1_length = -1
    word2_length = -1
    substitution_length = -1* np.sqrt(2)

    if c1 != c2:
        substitution_flag = True
    else:
        substitution_flag = False

    # skipping a character in word1
    word1_skip_return_tuple = np.add(
        r_edit_similarity_path_length(word1, word2, i - 1, j, value),
        (word1_skip_value, word1_length),
    )
    # skipping a character in word2
    word2_skip_return_tuple = np.add(
        r_edit_similarity_path_length(word1, word2, i, j - 1, value),
        (word2_skip_value, word2_length),
    )
    # substitution
    substitution_return_tuple = r_edit_similarity_path_length(
        word1, word2, i - 1, j - 1, value
    )
    substitution_return_tuple = np.add(
        substitution_return_tuple,
        (
            substitution_value if substitution_flag else no_substitution_value,
            substitution_length,
        ),
    )

    return max(
        [
            (0, 0),
            word1_skip_return_tuple,
            word2_skip_return_tuple,
            substitution_return_tuple,
        ],
        key=itemgetter(0),
    )


def length_of_similar_suffix(a, b):
    n = min((len(a), len(b)))
    return np.max([i for i in range(1, n + 1) if a[-i:] == b[-i:]] + [0])


def main(sargs):
    word1 = sargs[0]
    word2 = sargs[1]
    v = edit_similarity(word1, word2)
    print(v)
    print(length_of_similar_suffix(word1, word2))


if __name__ == "__main__":
    main(sys.argv[1:])

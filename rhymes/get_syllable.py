from hyphenate import hyphenate_word
import numpy as np
import hyphenate
import sys


def get_syllable_count(s):
    return sum([len(hyphenate_word(word)) for word in s.split()])


def get_syllable(words, count):
    return np.array(
        [word for word in words if len(np.array(hyphenate_word(word))) == count]
    )


if __name__ == "__main__":
    # word = sys.argv[1]
    # count = int(sys.argv[2])
    print(get_syllable_count("Cookin' up grams with arm and hammer."))
    print(get_syllable(["cat", "bat"], 1))

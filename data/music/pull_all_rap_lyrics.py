import get_lyrics
import sys

def pull_all_rappers(artist_directory,lyrics_directory,songs_per_artist):
    rappers = {}

    with open(artist_directory,'r') as f:
        for line in f:
            artist,coast = line.split(' :')
            rappers[artist]=coast.strip().lower()

    for name in rappers:
        get_lyrics.get_songs(name,songs_per_artist,lyrics_directory+rappers[artist]+"_coast/")

def main(args):
    if len(args) == 3:
        artist_directory = args[0]
        lyrics_directory = args[1]
        songs_per_artist = int(args[2])
        pull_all_rappers(artist_directory,lyrics_directory,songs_per_artist)
    else:
        print(' </directory_to_read_in/>  </directory_to_save_in/> <number_of_songs>')
if __name__ == "__main__":
    main(sys.argv[1:])
try:
  import lyricsgenius as genius #calling the API
except Exception as e:
  print("try :$python3 -m pip install lyricsgenius")
import sys

usage='<"Artist_Name"> <number_of_songs> </directory_to_save_in/>'

api=genius.Genius('cicqL9GuSORmoGQEIGCJdWew-qOFbpK8zSikTUiuTNfDD8mq0zpw6arffxPNyb3z', remove_section_headers=True, skip_non_songs=True)

def get_songs(name, num, directory):
  try:
    artist=api.search_artist(name, max_songs=num, sort='popularity')
    aux=artist.to_dict()

    titles=[song['title'] for song in aux['songs']]
    lyrics=[song['lyrics'] for song in aux['songs']]

    size = len(titles)

    name = name.replace(" ","_").replace('"','').replace("'","").replace(",","").replace(".","")
    name = ''.join([i if ord(i) < 128 else '' for i in name])

    for i in range(0,size):
      title = str(titles[i]).replace(" ","_").replace('"','').replace("'","").replace(",","").replace(".","")
      title = ''.join([i if ord(i) < 128 else '' for i in title])

      print(title)

      with open(str(directory)+str(name)+"__"+str(title)+".txt","w") as output:
        output.write(str(lyrics[i]))
  except:
    print("Unable to find Lyrics")

def main(args):
  if len(args) < 3:
    print(usage)
    exit(0)

  name=args[0]
  num=int(args[1])
  directory=args[2]
  get_songs(name, num, directory)

if __name__=='__main__':
  main(sys.argv[1:])

import nltk

grammar = nltk.CFG.fromstring("""  S -> NP VP | TO VB
VP -> V NP | V NP PP | V S | V PP
PP -> P NP  
V -> "caught" | "ate" | "likes" | "like" | "chase" | "go"
NP -> Det N | Det N PP | PRP
Det -> "the" | "a" | "an" | "my" | "some"
N -> "mice" | "cat" | "dog" |  "school"
P -> "in" | "to" | "on"
TO -> "to"
PRP -> "I"  """) 

sentence = 'I like my dog'
words = nltk.word_tokenize(sentence)

rdp = nltk.RecursiveDescentParser(grammar)
for tree in rdp.parse(words):
    print(tree)


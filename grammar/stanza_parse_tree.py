# Prints the cfg of a text
from stanza.server import CoreNLPClient
# Resource:  https://stanfordnlp.github.io/stanza/client_usage.html


def traverse_grammar_tree(data):
    if len(data.child) > 0:
        print(data.value, " -> ", [c.value for c in data.child])
        for child in data.child:
            traverse_grammar_tree(child)


if __name__ == '__main__':
    grammar_counter = {}
    text = "Chris Manning is a nice person. Chris wrote a simple sentence. He also gives oranges to people."
    # annotate grammar of text
    with CoreNLPClient(
            annotators=['tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse', 'depparse', 'coref'],
            timeout=30000,
            memory='16G') as client:
        ann = client.annotate(text)

    # create CFG of text
    for s in ann.sentence:
        constituency_parse = s.parseTree
        traverse_grammar_tree(constituency_parse.child[0])

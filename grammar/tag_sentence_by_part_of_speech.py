import nltk

sentence = 'My friend, Ishmael, had the best of Xergergs and the worst of Xergers.'
words = nltk.word_tokenize(sentence)

# print parts of speech for each word
tagged_words = nltk.pos_tag(words)
print(tagged_words)

# get proper nouns
proper = nltk.chunk.ne_chunk(tagged_words)
print(proper)

from stanza.server import CoreNLPClient
from collections import Counter
import sys
import random
import re
from datasketch import MinHash, MinHashLSH
import nltk
import gensim
from nltk.data import find


class GrammarNode:

    def __init__(self, label, original_sentence_num):
        self.label = label
        self.children = []
        self.sentence_num = original_sentence_num

    def add_children(self, children_list):
        self.children = children_list


def map_word_to_pos(data, prev_data, pos_to_word):
    if len(data.child) > 0:
        for child in data.child:
            map_word_to_pos(child, data, pos_to_word)
    elif prev_data.value != "NNP":
        if prev_data.value not in pos_to_word.keys():
            pos_to_word[prev_data.value] = set()
        pos_to_word[prev_data.value].add(data.value)


def grammar_tree_to_string(parent, tree_string, i, k):
    # traverse and build string
    tree_string += parent.label + " "
    if len(parent.children) > 0 and i <= k:
        tree_string += "( "
        for child_node in parent.children:
            tree_string = grammar_tree_to_string(child_node, tree_string, i + 1, k)
        tree_string += ")"
    return tree_string


def add_children_nodes(stanza_parent, parent, i, k, sentence_num, prev_parent, nouns):
    if i <= k:
        children_list = []
        for stanza_child in stanza_parent.child:
            child = GrammarNode(stanza_child.value, sentence_num)
            children_list.append(child)
            add_children_nodes(stanza_child, child, i + 1, k, sentence_num, parent, nouns)
        parent.add_children(children_list)
        if len(children_list) == 0 and prev_parent.label in ["NN", "NNP", "PRP", "PRP$", "NNS"]:
            nouns.add(parent.label)


def k_prev_layers_to_children_helper(stanza_parent, tree_dist, k, sentence_num, prev_parent, nouns):
    parent = GrammarNode(stanza_parent.value, sentence_num)
    add_children_nodes(stanza_parent, parent, 1, k, sentence_num, prev_parent, nouns)
    # only want grammar string of k-1
    tree_string = grammar_tree_to_string(parent, "", 1, k - 1)
    if tree_string not in tree_dist.keys():
        tree_dist[tree_string] = Counter()
    tree_dist[tree_string][parent] += 1
    for stanza_child in stanza_parent.child:
        k_prev_layers_to_children_helper(stanza_child, tree_dist, k, sentence_num, parent, nouns)


def k_prev_layers_to_children(stanza_parent, tree_dist, k, sentence_num, lsh):
    minhash = MinHash(num_perm=128)
    nouns = set()
    parent = GrammarNode(stanza_parent.value, sentence_num)
    add_children_nodes(stanza_parent, parent, 1, k, sentence_num, None, nouns)
    tree_string = grammar_tree_to_string(parent, "", 1, k - 1)
    # add to prior
    tree_dist["S"][parent] += 1
    # add to dist
    if tree_string not in tree_dist.keys():
        tree_dist[tree_string] = Counter()
    tree_dist[tree_string][parent] += 1
    for child in stanza_parent.child:
        k_prev_layers_to_children_helper(child, tree_dist, k, sentence_num, parent, nouns)
    # use nouns and pronouns from sentence for LSH
    for word in nouns:
        minhash.update(word.encode('utf-8'))
    lsh.insert(sentence_num, minhash)


def create_distribution(tree_counter):
    for node in tree_counter.keys():
        tree_counter[node] = dict(tree_counter[node])
        cur_dict = tree_counter[node]
        total = sum(cur_dict.values())
        for sub_node in cur_dict.keys():
            cur_dict[sub_node] /= total


def sample_from_distribution(dist):
    tree_nodes = [k for k in dist.keys()]
    distribution = [p for p in dist.values()]
    random_tuple = random.choices(tree_nodes, distribution)
    return random_tuple


def generate_sentence_helper(parent, tree_dist, leaves, k, prev_parent, lsh_options, sentence_nouns,
                             leaves_pos):
    new_parent = sample_from_distribution(tree_dist[grammar_tree_to_string(parent, "", 1, k)])[0]
    if len(lsh_options) > 0:
        while new_parent.sentence_num not in lsh_options:
            new_parent = sample_from_distribution(tree_dist[grammar_tree_to_string(parent, "", 1, k)])[0]
    parent = new_parent
    if len(parent.children) == 0:
        if prev_parent.label in ["NN", "NNP", "PRP", "PRP$", "NNS"]:
            # for sentence LSH
            sentence_nouns.add(parent.label)
        leaves.append(parent.label)
        leaves_pos.append(prev_parent.label)
    else:
        for child in parent.children:
            generate_sentence_helper(child, tree_dist, leaves, k, parent, lsh_options, sentence_nouns,
                                     leaves_pos)


def pick_best_replacement_word_pair(w1, w2, w1_given, w1_replacements, w2_replacements, wordnet):
    word_pair_similarities = []
    try:
        pair_sim = wordnet.similarity(w1, w2)
        if w1_given is not None:
            w1 = w1_given
        for w1_replacement in w1_replacements:
            if w1_replacement != w1:
                try:
                    replacement_pair_sim = wordnet.similarity(w1_replacement, w2)
                    word_pair_similarities.append(((w1_replacement, w2), replacement_pair_sim))
                except KeyError:
                    pass
            for w2_replacement in w2_replacements:
                try:
                    replacement_pair_sim = wordnet.similarity(w1, w2_replacement)
                    word_pair_similarities.append(((w1, w2_replacement), replacement_pair_sim))
                except KeyError:
                    pass
                try:
                    replacement_pair_sim = wordnet.similarity(w1_replacement, w2_replacement)
                    word_pair_similarities.append(((w1_replacement, w2_replacement), replacement_pair_sim))
                except KeyError:
                    pass
    except KeyError:
        pass
    if len(word_pair_similarities) == 0:
        return (w1, w2)
    word_pair_similarities.sort(key=lambda x: abs(x[1] - pair_sim))
    # check if replacement words from the best pair make sense
    # a replacement word must be somewhat related to original word (>0.1 similarity)
    w1_replace, w2_replace = word_pair_similarities[0][0][0], word_pair_similarities[0][0][1]
    if wordnet.similarity(w1_replace, w1) < 0.1:
        w1_replace = w1
    if wordnet.similarity(w2_replace, w2) < 0.1:
        w2_replace = w2
    return (w1_replace, w2_replace)


def get_most_similar_word(word, replacements, wordnet):
    # use higher similarity threshold for replacing a single word
    max_sim = 0.15
    best_word = word
    for replacement_word in replacements:
        try:
            word_sim = wordnet.similarity(replacement_word, word)
            if word_sim > max_sim:
                max_sim = word_sim
                best_word = replacement_word
        except KeyError:
            pass
    return best_word


def generate_sentence(tree_dist, k, lsh_options, prev_sentence_start, pos_to_theme_words,
                      theme_tuning, wordnet, word_to_replacement):
    parent = sample_from_distribution(tree_dist["S"])[0]
    if len(lsh_options) > 0:
        # check if this is a valid parent based on lsh options
        while parent.sentence_num not in lsh_options or parent.sentence_num == prev_sentence_start:
            parent = sample_from_distribution(tree_dist["S"])[0]
    leaves = []
    leaves_pos = []
    sentence_nouns = set()
    for child in parent.children:
        generate_sentence_helper(child, tree_dist, leaves, k, parent, lsh_options, sentence_nouns, leaves_pos)

    context_words = []
    context_words_pos = []
    context_words_indices = []
    replacement_words = []

    if theme_tuning:
        # find and replace words based on given theme words
        theme_pos = set(pos_to_theme_words.keys())
        # only replace nouns and adjectives in the sentence
        theme_pos = theme_pos.intersection({"NN", "NNS", "JJ"})
        for i in range(len(leaves)):
            word, pos = leaves[i], leaves_pos[i]
            if pos in theme_pos:
                context_words.append(word)
                context_words_pos.append(pos)
                context_words_indices.append(i)

    if len(context_words) > 1:
        # pick replacements for context words based on pair and word similarity
        # pick first pair
        w1, w2 = context_words[0], context_words[1]
        best_word_pair = pick_best_replacement_word_pair(w1, w2, None, pos_to_theme_words[context_words_pos[0]],
                                                         pos_to_theme_words[context_words_pos[1]],  wordnet)
        word_to_replacement[w1] = best_word_pair[0]
        given_word = best_word_pair[1]
        word_to_replacement[w2] = given_word
        replacement_words = [best_word_pair[0], best_word_pair[1]]
        # given first pair of words, slide pair window over by one and pick next word
        for i in range(1, len(context_words) - 1):
            best_word_pair = pick_best_replacement_word_pair(context_words[i], context_words[i+1], given_word,
                                                             {given_word}, pos_to_theme_words[context_words_pos[i+1]], wordnet)
            given_word = best_word_pair[1]
            replacement_words.append(given_word)
            word_to_replacement[context_words[i + 1]] = given_word
    elif len(context_words) == 1:
        # pick most similar word from theme words
        replacement_words = [get_most_similar_word(context_words[0], pos_to_theme_words[context_words_pos[0]], wordnet)]

    if len(replacement_words) > 0:
        # replace context words with their best found theme word
        replacement_leaves = leaves.copy()
        for i in range(len(context_words_indices)):
            index = context_words_indices[i]
            replacement_leaves[index] = replacement_words[i]
        sentence = " ".join(replacement_leaves)
    else:
        sentence = " ".join(leaves)
    # fix punctuation spacing and capitalize first letter
    sentence = re.sub(r'\s([?.,;:!](?:\s|$))', r'\1', sentence.capitalize())
    print(re.sub(r"\b\s+'\b", r"'", sentence))
    return sentence_nouns, parent.sentence_num


def generate_sentences_from_features_helper(sentence_dist, cur_num_of_sentences, target_num_of_sentences, k, lsh, prev_lsh_options,
                                            prev_sentence_start, pos_to_theme_words, theme_tuning, wordnet, word_to_replacement):
    if cur_num_of_sentences < target_num_of_sentences:
        # generate sentence for that feature
        sentence_nouns, prev_sentence_start = generate_sentence(sentence_dist, k, prev_lsh_options, prev_sentence_start,
                                                                pos_to_theme_words, theme_tuning, wordnet, word_to_replacement)
        # LSH new sentence
        minhash = MinHash(num_perm=128)
        for word in sentence_nouns:
            minhash.update(word.encode('utf-8'))
        lsh_options = lsh.query(minhash)
        if len(lsh_options) == 0:
            lsh_options = prev_lsh_options
        lsh_options = [i + 1 for i in lsh_options]
        cur_num_of_sentences += 1
        generate_sentences_from_features_helper(sentence_dist, cur_num_of_sentences, target_num_of_sentences, k, lsh, lsh_options,
                                                prev_sentence_start, pos_to_theme_words, theme_tuning, wordnet, word_to_replacement)


def generate_sentences_from_features(sentence_dist, target_num_of_sentences, k, lsh, pos_to_theme_words, theme_tuning, wordnet):
    cur_num_of_sentences = 0
    # starting sentence
    lsh_options = []
    word_to_replacement = {}
    sentence_nouns, prev_sentence_start = generate_sentence(sentence_dist, k, lsh_options, -1, pos_to_theme_words,
                                                            theme_tuning,wordnet, word_to_replacement)
    cur_num_of_sentences += 1
    # LSH new sentence
    minhash = MinHash(num_perm=128)
    for word in sentence_nouns:
        minhash.update(word.encode('utf-8'))
    new_lsh_options = lsh.query(minhash)
    if len(new_lsh_options) > 0:
        lsh_options = new_lsh_options
    lsh_options = [i + 1 for i in lsh_options]
    generate_sentences_from_features_helper(sentence_dist, cur_num_of_sentences, target_num_of_sentences, k, lsh, lsh_options,
                                            prev_sentence_start, pos_to_theme_words, theme_tuning, wordnet, word_to_replacement)


def get_stanza_tree(client, sentence):
    ann = client.annotate(sentence.replace('"', ''))
    return ann.sentence[0]


def main(args):
    # python3 stanza_cfg_sentence_generator.py 3 3 ../data/books/emma_short.txt supernatural vampires blood
    if len(args) >= 2:
        k_tree = int(args[0])  # how many tree layers to look back
        fname = args[1]
        theme_tuning = False
        if len(args) >= 3:
            theme_words = args[2:]
            theme_tuning = True

        theme_words_set = set()
        pos_to_theme_words = {}
        model = None
        if theme_tuning:
            word2vec_sample = str(find('models/word2vec_sample/pruned.word2vec.txt'))
            model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_sample, binary=False)
            for word in theme_words:
                theme_words_set.add(word)
                try:
                    related_words = model.most_similar(positive=[word], topn=5)
                    for w in related_words:
                        theme_words_set.add(w[0])
                except KeyError:
                    print(word, "not in wordnet")
                    exit()
            related_words = model.most_similar(positive=theme_words, topn=10)
            for w in related_words:
                theme_words_set.add(w[0])

        sentence_dist = {"S": Counter()}
        # split text into sentences to be annotated
        lines = open(fname).readlines()[1:]
        text = '\n'.join(lines)
        sentences = nltk.sent_tokenize(text)
        with CoreNLPClient(
                annotators=['tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse', 'depparse', 'coref'],
                timeout=300000,
                memory='16G') as client:
            for word in theme_words_set:
                ann = client.annotate(word)
                s = ann.sentence[0]
                constituency_parse = s.parseTree
                map_word_to_pos(constituency_parse.child[0], None, pos_to_theme_words)
            annotated_sentences = [client.annotate(s.replace('"', '')).sentence[0] for s in sentences]

        # higher the threshold, the less "bins" sentences will go into
        lsh = MinHashLSH(threshold=0.2, num_perm=128)

        sentence_num = 0
        for s in annotated_sentences:
            constituency_parse = s.parseTree
            k_prev_layers_to_children(constituency_parse.child[0], sentence_dist, k_tree, sentence_num, lsh)
            sentence_num += 1

        create_distribution(sentence_dist)

        sentences_to_generate = 25
        print("Theme words:")
        print(pos_to_theme_words)
        print("Text:")
        generate_sentences_from_features(sentence_dist, sentences_to_generate, k_tree, lsh, pos_to_theme_words,
                                         theme_tuning, model)
    else:
        print('usage: stanza_cfg sentences.txt k_tree fname theme_words ...')


if __name__ == '__main__':
    main(sys.argv[1:])

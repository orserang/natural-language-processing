\documentclass[12pt]{book}

\usepackage{hyperref}

\usepackage{graphicx}
\usepackage{array}
\usepackage{import}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\newcommand{\pmf}{\DOTSB\mbox{pmf}}

\usepackage[titles]{tocloft}
\makeatletter
\newcommand*{\tocwithouttitle}{\@starttoc{toc}}
\makeatother

\usepackage{sectsty}
\partfont{\Large\uppercase}

\usepackage{listings}
\usepackage{fontawesome}
% \defcaptionname{english}{\lstlistingname}{Algorithm}

\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true
  tabsize=3
}

\usepackage{setspace}

\sloppy

\begin{document}

\title{A brief introduction to formal language and grammar}
\author{Oliver Serang}
\maketitle

\section*{Introduction}

Formal languages and grammars are ways to discuss sets of strings that
have a shared property just as number theory would discuss sets of
numbers that share a property (\emph{e.g.}, odd numbers, prime
numbers, \emph{etc.}).

\section*{$\Sigma$: the alphabet}
Let's use $\Sigma$ to denote the set of characters in our
alphabet. For example, if we're considering binary strings, then
$\Sigma=\{0,1\}$. 

\section*{The Kleene star}
The * operator defines the set of strings with any number of copies
(including 0 copies) of the preceding argument; therefore, if $\Sigma$
is the set of characters in our alphabet, $\Sigma*$ represents the set
of all strings with 0 contiguous copies of any characters from
$\Sigma$ (let's call this empty string $\epsilon$), with 1 contiguous
copies of any characters from $\Sigma$, and so on. Note that this unit
does not need to be homogenous: if $\Sigma=\{a,b,c\}$, then
$\Sigma*=\{\epsilon, a, b, c, aa, ab, ac, ba, bb, bc, ca, cb, cc, aaa,
\ldots\}$. Clearly $\Sigma*$ is the set of all possible strings of any
length that can be made with our alphabet $\Sigma$.

\section*{Language}
A language $L$ is some specific subset of $\Sigma*$. For example, a language could allow any strings of a given length, any strings of prime length, any binary strings that contain an equal number of 0 and 1 characters, and so on. This definition of languages is very permissive and doesn't specify \emph{how} we must define the set, only that the we must be formal and clear about which strings it includes. For this reason, there are different concrete ways to define languages, some of which are more powerful than others. 

\section*{Grammar}
A grammar is one formal way to define a language. Specifically, a
grammar defines non-terminal characters (in addition to the terminal
characters, which compose $\Sigma$) and uses strings with the
non-terminal characters and terminal characters to essentially perform
find-replace operations.

For example, consider the language based on $\Sigma=\{(,)\}$ and where an equal number of ( and ) characters must be used in a balanced manner. \emph{I.e.}, $((\not\in L$, $()) \not\in L$, $)( \not\in L$, but $(()())(()) \in L$. Note that thus far, out description of this language has been informal and relies on examples and our own extant intuition.

We can formally describe this language as follows. Let us first define
when a string has balanced parentheses. Let ( characters be replaced
by a -1 and ) characters be replaced by +1, so that $(()()(()))$ would
become $[+1,+1,-1,+1,-1,+1,+1,-1,-1,-1]$. Thus we can formally define balanced parentheses:

\[
bal(parstr) =
\begin{cases}
  true & \text{all cumulative left-to-right sums are }\geq 0\text{ and total sum}=0\\
  false & \text{else}.
\end{cases}
\]

Then $L = \{ s \in \{(,)\}* : bal(s) \}$. 

We can define this language using the following grammar:
\begin{eqnarray*}
  S &\rightarrow& (S)\\
  S &\rightarrow& SS\\
  S &\rightarrow& \epsilon
\end{eqnarray*}

Note that we will use capital letters $A, B, ..., S, ...$ for
non-terminal symbols and lowercase letters (either English or Greek)
for terminal symbols. Since we only replace non-terminal symbols with
new expressions (containing combinations of terminal and non-terminal
symbols), then we can no longer apply production rules when we have
only terminal characters. Likewise, no string with non-terminal
characters can be in our langauge, since $\Sigma$ contains only
terminal characters.

The single right arrow in the production rule $S\rightarrow (S)$ means
we \emph{may} replace $S$ with $(S)$ if we would like. If we want to
indicate that we \emph{do} such a rplacement, then we use the double
arrow: {\tt S $\Rightarrow$ (S)}. We read this as ``$S$ yields
$(S)$.''  If some number of such production rules can be applied to
produce string $\Delta$ from string $\Gamma$, then we write $\Gamma
\Rightarrow \cdots \Rightarrow \Delta$ or equivalently that $\Gamma
\Rightarrow* \Delta$, which is read as ``$\Delta$ is derived from
$\Gamma$.''

We can write this in a more compressed form:
\begin{eqnarray*}
  S &\rightarrow& (S) | SS | \epsilon
\end{eqnarray*}

\subsection*{Regular languages}
Regular languages are built by by allowing stretches of several
characters from subsets of $\Sigma$.

For example, we can generate the language of integers using the
regular expression {\tt [0-9]+}, where {\tt [0-9]} is shorthand for
the set of characters {\tt 0}, {\tt 1}, {\tt 2}, \ldots, {\tt 9} and
the {\tt +} is shorthand for ``one or more copies.'' It can be written
with a Kleene star as {\tt [0-9][0-9]*}.

Grammars for regular languages include
\begin{eqnarray*}
  A &\rightarrow& aB
\end{eqnarray*}
and
\begin{eqnarray*}
  A &\rightarrow& Ba
\end{eqnarray*}
Note that regular grammars should not allow us to mix terminal
characters emitted on the left and right of a non-terminal character;
all rules should put the terminal character on the same side. Also
note that $A$ is allowed to be replaced with $Ba$ for any $B$,
including $B=A$.

For example, the integers can be generated using this grammar:
\begin{eqnarray*}
  A &\rightarrow& B0 | B1 | B2 | \cdots B9\\
  B &\rightarrow& B0 | B1 | B2 | \cdots B9 | \epsilon\\
\end{eqnarray*}

\subsubsection*{Deterministic finite automata (DFA)}
It turns out that we can generate regular languages using
deterministic finite automata (DFA). For instance, we could enter into
state $A$, which transitions to state $B$ by emitting any character
from {\tt [0-9]}. State $B$ can transition to itself by emitting
characters from {\tt [0-9]}, or can emit an $\epsilon$ empty string
and transition to the end state. States that loop back to themselves
allow any number of some subset of characters to be emitted in a
string of arbitrarily large length.

\subsubsection*{From DFA to parser}
We can turn a DFA into a parser by inverting character emission with
character reading. Where a DFA emitting characters can emit character
$\alpha$ when transitioning from state $S$ to state $T$ and also emit
character $\alpha$ when transitioning from state $S$ to state $U$, a
DFA reading characters will require the character to uniquely
determine the transition. In this example, reading character $\alpha$
while in state $S$ must uniquely determine the transition to either
$T$ or $U$. This can be accomplished by ``factoring out'' any
character with a non-unique transition from $S$, \emph{i.e.},
$\alpha$, so that in state $S$, character $\alpha$ transitions to
state $V$. Then any characters not emitted by both $T$ and $U$ will be
unambiguous, and so can attach the edge from $V$ that they had from
either $T$ or $V$. Any characters emitted when transitioning from both
$T$ and $U$ will need an edge in $V$. If the edges from $T$ and $U$
for character $\beta$ go to the same destination state, then the edge
can be included from $V$ without change; but if the edges from $T$ and $U$ emitting $\beta$ go to states $W$ and $X$ respectively, then the process continues, with a new state $Y$ needed to remove the ambiguity between $W$ and $X$. 

The process is continued in this manner, adding only a finite number
of auxiliary states, because ambiguities in transitions to pairs of
genuine states may introduce an auxiliary state while removing the
ambiguity.

\subsection*{Context-free languages}
Regular languages cannot define such constructs as balanced
parentheses. To demonstrate this, use the bijection from a regular
langauge to a DFA. The start state of the DFA, $S_0$, cannot accept
the character {\tt )}, because there have not yet been any {\tt (}
characters. {\tt (} will transition from $S_i$ to $S_{i+1}$; $S_1$ can
now accept one {\tt )}, but no more; therefore, it cannot transition
to itself as we saw with the Kleene star pattern from regular
languages. As a result, the states themselves must keep a memory of
how deep we are in parenthetical expressions. If we want to support
arbitrarily deep parenthetical expressions, we will require an
infinite number of states.

Balanced parentheses are an example of a context-free language (a
superset of regular languages). Context-free languages have production
rules that can replace a non-terminal character any combination of
non-terminal and terminal characters.
\begin{eqnarray*}
  A &\rightarrow& abABbaA
\end{eqnarray*}

\subsubsection*{Push-down automata}
Context-free languages include finding balanced parentheses; for this
reason, parsers that accept them need to keep track of an arbitrary
amount of state (rather than finite state). Balanced parenthetical
expressions can be accepted using an aribtrary-precision integer
counter, which counts the depth of the parentheses. This counter is
adjusted during transitions from one state to the next: it is
incremented when {\tt (} is read and decremented when {\tt )} is read.

Furthermore, the characters that can be read during a transition need
to see the state of that integer. For this reason, we see that the
transitions depend on this additional state.

Rather than use a finite number of arbitrarily large integers on which
the state machine will depend, we can instead use a stack on which we
push finite chunks of data. Our automata can read and transition
depending not only on its finite state (as in a DFA) but also using
the stack. Such an automata is called a ``push-down automata.'' It is more powerful than a DFA. 

\subsubsection*{Languages with hierarchical objects}
Context-free languages represent hierarchical objects, such as in the
example of balanced parentheses. We can likewise make a context-free
grammar to generate a language with balanced parentheses {\tt ()} as
well as brackets {\tt []}. Within an object (the object is indicated
by a non-terminal symbol at the left-hand side of a production rule),
we can force balanced parentheses as well as brackets; however, this
does not allow cross-talk between far-away objects. The language of
balanced parentheses and brackets that form nested expressions with
one another (\emph{e.g.}, {\tt ([]()[([])])}) is context-free; but the
language of balanced parentheses and brackets arranged
non-hierarchically (\emph{e.g.}, {\tt (([))]}) is not context-free;
for this we need a context-sensitive language.

\subsection*{Context-sensitive languages}
Context-sensitive languages relax the restriction that the left-hand
side of production rules need to be single non-terminal characters. For instance
\begin{eqnarray*}
  aAB &\rightarrow& C
\end{eqnarray*}
is a context-sensitive grammar.

Context-sensitive grammars are much more broad than context-free
grammars, and do not need to represent objects with a nested pattern;
however, they are much more difficult to parse. For this reason, they
are often avoided for reasons of computational efficiency.

\subsection*{Other languages}
Context-sensitive languages are not the most powerful possible
grammars; for that, we would simply return to our compact (but
daringly computationally inefficient) set notation, defining all
strings from an alphabet that return {\tt true} when passed through an
arbitrary function (using an imperative language).

\end{document}


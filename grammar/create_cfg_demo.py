# Prints the cfg of a text
from stanza.server import CoreNLPClient
from collections import Counter
# Resource:  https://stanfordnlp.github.io/stanza/client_usage.html


def traverse(data, grammar_counter):
    if len(data.child) > 0:
        print(data.value, " -> ", [c.value for c in data.child])
        if data.value not in grammar_counter.keys():
            grammar_counter[data.value] = Counter()
        grammar_counter[data.value][tuple([c.value for c in data.child])] += 1
        for child in data.child:
            traverse(child, grammar_counter)


if __name__=='__main__':
    grammar_counter = {}
    text = "Chris Manning is a nice person. Chris wrote a simple sentence. He also gives oranges to people."
    # annotate grammar of text
    with CoreNLPClient(
            annotators=['tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse', 'depparse', 'coref'],
            timeout=30000,
            memory='16G') as client:
        ann = client.annotate(text)

    # create CFG of text
    for s in ann.sentence:
        print("NEW SENTENCE")
        constituency_parse = s.parseTree
        # print CFG of sentence
        traverse(constituency_parse.child[0], grammar_counter)
    # print(grammar_counter)

    # create dist of cfg
    for key in grammar_counter.keys():
        grammar_counter[key] = dict(grammar_counter[key])
        rule = grammar_counter[key]
        summ = sum(rule.values())
        for next_rule in rule.keys():
            rule[next_rule] /= summ
    # print(grammar_counter)

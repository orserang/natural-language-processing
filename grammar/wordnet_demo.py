# Resource: https://www.kaggle.com/jihyeseo/word2vec-gensim-play-look-for-similar-words
import sys
import gensim
from nltk.data import find


def main(args):
    if len(args) >= 1:
        words = args

        # get gensim wordnet
        word2vec_sample = str(find('models/word2vec_sample/pruned.word2vec.txt'))
        model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_sample, binary=False)

        for word in words:
            try:
                related_words = model.most_similar(positive=[word], topn=5)
                print(word, ":")
                print([w[0] for w in related_words])
            except KeyError:
                print(word, "not in wordnet")

        try:
            related_words = model.most_similar(positive=words, topn=10)
            print("combined word similarity:")
            print([w[0] for w in related_words])
        except KeyError:
            print("some of the words are not in the wordnet")
    else:
        print('usage: wordnet_demo space planet alien ...')


if __name__ == '__main__':
    main(sys.argv[1:])
import numpy as np
import pylab as P
from collections import Counter
import itertools
import nltk
import sys
from nltk.book import *

def load_sentences(fname):
  # skip first line (with tag information)
  lines = open(fname).readlines()[1:]
  data = '\n'.join(lines)
  sentences = nltk.sent_tokenize(data)
  return list(sentences)

def estimate_prior(all_books_sentences):
  # fixme
  pass

def estimate_transition_matrix(all_books_sentences):
  # fixme
  pass

def estimate_sentence_length_dist(all_books_sentences):
  # fixme
  pass

def generate_text(k_word_and_prior_prob_s, k_prev_words_to_next_word_and_prob, sentence_length_and_final_k_words_to_prob, num_sentences_to_generate):
  # fixme
  pass

def main(args):
  if len(args) >= 3:
    corpus_fnames = args[:-2]
    # order k markov model looks back at k previous words:
    k = int(args[-2])
    num_sentences_to_generate = int(args[-1])

    all_books_sentences = [ load_sentences(f) for f in corpus_fnames ]
    k_word_and_prior_prob_s = estimate_prior(all_books_sentences)
    k_prev_words_to_next_word_and_prob = estimate_transition_matrix(all_books_sentences)
    sentence_length_and_final_k_words_to_prob = estimate_sentence_length_dist(all_books_sentences)

    generate_text(k_word_and_prior_prob_s, k_prev_words_to_next_word_and_prob, sentence_length_and_final_k_words_to_prob, num_sentences_to_generate)

  else:
    print('usage: <corpus_1.txt> [corpus_2.txt...] <k_order_of_markov_model> <num_sentences_to_generate>')

if __name__ == '__main__':
  main(sys.argv[1:])

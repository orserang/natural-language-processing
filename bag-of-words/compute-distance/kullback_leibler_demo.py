import sys
import nltk
from collections import Counter
import math
import itertools

MINIMUM_FREQUENCY = 1e-8

def distance(word_to_count_a, word_to_count_b):
  all_words = set(word_to_count_a) | set(word_to_count_b)

  total_words_a = sum(word_to_count_a.values())
  total_words_b = sum(word_to_count_b.values())

  tot = 0.0
  for w in all_words:
    freq_a = freq_b = 0.0
    if w in word_to_count_a:
      freq_a = word_to_count_a[w] / total_words_a
    if w in word_to_count_b:
      freq_b = word_to_count_b[w] / total_words_b

    freq_a = max(freq_a, MINIMUM_FREQUENCY)
    freq_b = max(freq_b, MINIMUM_FREQUENCY)
    tot += freq_a*math.log(freq_a/freq_b) + freq_b*math.log(freq_b/freq_a)
    
  return tot

def load_bag_of_words(fname):
  # skip first line (with tag information)
  lines = open(fname).readlines()[1:]
  data = '\n'.join(lines)

  sentences = nltk.sent_tokenize(data)
  words = itertools.chain(*[ nltk.word_tokenize(s) for s in sentences ])
  return Counter([ w.upper() for w in words ])

def main(args):
  if len(args) == 2:
    fname_a, fname_b = args
    word_to_count_a = load_bag_of_words(fname_a)
    word_to_count_b = load_bag_of_words(fname_b)
    
    print(distance(word_to_count_a, word_to_count_b))

  else:
    print('usage: distance <file_a> <file_b>')

if __name__=='__main__':
  main(sys.argv[1:])

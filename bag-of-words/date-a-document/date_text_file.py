import nltk
import itertools
from collections import Counter
import sys

# Load the ngram files. They are large, so you may not be able to hold all of them in RAM simultaneously.

# Compute the product over independent distributions of each word's
# ngram distribution in time (the product being over each word present
# in the file argument to this program), then taking the mode of that
# resulting product distribution.

def load_bag_of_words(fname):
  # skip first line (with tag information)
  lines = open(fname).readlines()[1:]
  data = '\n'.join(lines)

  sentences = nltk.sent_tokenize(data)
  words = itertools.chain(*[ nltk.word_tokenize(s) for s in sentences ])
  return Counter([ w.upper() for w in words ])

def date_document(word_to_count):
  # fixme
  pass

def main(args):
  if len(args) == 1:
    fname = args[0]

    year_to_probability = date_document(load_bag_of_words(fname))

    # fixme: plot year vs. probability file is from that year

  else:
    print('usage: date <filename>')

if __name__=='__main__':
  main(sys.argv[1:])

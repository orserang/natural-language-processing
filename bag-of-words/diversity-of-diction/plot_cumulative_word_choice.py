import numpy as np
import pylab as P
from collections import Counter
import itertools
import nltk
import sys
from nltk.book import *

def load_words(fname):
  # skip first line (with tag information)
  lines = open(fname).readlines()[1:]
  data = '\n'.join(lines)
  sentences = nltk.sent_tokenize(data)
  words = list(itertools.chain(*[ nltk.word_tokenize(s) for s in sentences ]))
  print(fname, 'has sentences of average len', np.mean([ len(s) for s in sentences]), 'and words of average len', np.mean([len(w) for w in words]))
  return words

def plot_cumulative_word_use(fnames, each_books_words, top_words=300):
  for f,words in zip(fnames,each_books_words):
    freqDist = FreqDist(words)
    t=sum(freqDist.values())
    y = np.cumsum(sorted([b/t for a,b in freqDist.items() ])[::-1][:top_words])
    P.plot(y,label=f)

  P.legend()
  P.show()

def main(args):
  fnames = args
  each_books_words = [ load_words(f) for f in fnames ]
  plot_cumulative_word_use(fnames, each_books_words)

if __name__ == '__main__':
  main(sys.argv[1:])

import numpy as np
import pylab as P
from collections import Counter
import itertools
import nltk
import sys
from nltk.book import *

def load_sentences(fname):
  # skip first line (with tag information)
  lines = open(fname).readlines()[1:]
  data = '\n'.join(lines)
  sentences = nltk.sent_tokenize(data)
  return list(sentences)

def get_metadata_as_dict(fname):
  # fixme
  pass

def generate_features(sentences):
  # fixme
  pass

def main(args):
  if len(args) >= 2:
    corpus_fnames = args[:-1]
    mystery_text_fname = args[-1]
    each_books_sentences = [ load_sentences(f) for f in corpus_fnames ]
    
    authors = [ get_metadata_as_dict(f)['Author'] for f in corpus_fnames ]
    each_books_features = [ generate_features(sentences) for sentences in each_books_sentences ]
    mystery_text_features = generate_features(load_sentences(unknown_book_fname))

    print_estimated_author(authors, each_books_features, mystery_text_features)
  else:
    print('usage: <corpus_1.txt> [corpus_2.txt...] <unknown_author.txt>')

if __name__ == '__main__':
  main(sys.argv[1:])
